# E503: Basic Machine Learning - Semester Project 
## A Comparative Study of Different Methods for Sentence to Sentence Semantic Similarity

> Date: 12/01/2022

## Abstract 
This project looks into different approaches to solve the problem of semantic similarity between any pair of sentences using computation. Comparison is done using a dataset provided by Quora for a competition on Kaggle. We also develop reasoning for why transformers work best for such types of problems.

Please see this [report](./E503%20Project%20Report%20-%20Sentence%20to%20Sentence%20Semantic%20Similarity.pdf) for details. 



