import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
import matplotlib.pyplot as plt 
import seaborn as sns
from collections import Counter
import re
from bs4 import BeautifulSoup
import inspect

import torch
from torch.utils.data import DataLoader
import math
from sentence_transformers import SentenceTransformer,  LoggingHandler, losses, models, util
from sentence_transformers.evaluation import EmbeddingSimilarityEvaluator
from sentence_transformers.readers import InputExample
import logging
from datetime import datetime
import sys
import os
import gzip
import csv

def retrieve_name(var):
    callers_local_vars = globals().items()
    return [var_name for var_name, var_val in callers_local_vars if var_val is var]

def print_df_name():
    print(f" ")
    print(f" ")
    print(f"{retrieve_name(df)}")

def show_bad_enteries( df: pd.DataFrame ):
    print(f"------------------")
    print(f"Null enteries stats")
    print(f"{df.isnull().sum()}")
    print(f"--------")
    df[df.isnull()]

def drop_bad_enteries( df: pd.DataFrame ) -> pd.DataFrame:
    df.dropna( axis = 0, inplace = True )
    return df    

##########################################################################
# Configurations 

#You can specify any huggingface/transformers pre-trained model here, for example, bert-base-uncased, roberta-base, xlm-roberta-base
model_name = "sentence-transformers/all-MiniLM-L6-v2"

# Read the dataset
train_batch_size = 16
num_epochs = 4
model_save_path = 'output/training_'+model_name.replace("/", "-")+'-'+datetime.now().strftime("%Y-%m-%d_%H-%M-%S")


##########################################################################
# Reading Data

df = []
def sanitize_data():
    dfs = [ train, test, test_kragel ]

    for df in dfs:
        print_df_name()
        show_bad_enteries( df )
        print(f"Sanitizing")
        drop_bad_enteries(df)
        show_bad_enteries( df )
        
paths = ['./data/train.csv', './data/test.csv', './data/test_kragel.csv']
dfs = []
for path in paths:
    dfs.append( pd.read_csv( path ) )

train = dfs[0]
test = dfs[1]
test_kragel = dfs[2]

sanitize_data()

train_samples = []
dev_samples = []
test_samples = []

def df_to_samples( df ):
    samples = []
    
    df = df.dropna()
    for i,r in df.iterrows():
        inp_example = InputExample(texts=[r['question1'], r['question2']], label=float(r['is_duplicate']))
        
        def check_for_float( t ): 
            if isinstance( r[t], float ):
                print( r[t] )
        
        check_for_float('question1')
        check_for_float('question2')
        samples.append( inp_example )
        
    return samples 

train_samples = df_to_samples( train )
dev_samples = df_to_samples( test )

train_dataloader = DataLoader(train_samples, shuffle=True, batch_size=train_batch_size)
dev_dataloader = DataLoader(dev_samples, shuffle=True, batch_size=train_batch_size)

print( train_dataloader )
print( dev_dataloader )

##########################################################################
# Building Model 

#### Just some code to print debug information to stdout
logging.basicConfig(format='%(asctime)s - %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S',
                    level=logging.INFO,
                    handlers=[LoggingHandler()])



# Device configuration
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

# Use Huggingface/transformers model (like BERT, RoBERTa, XLNet, XLM-R) for mapping tokens to embeddings
word_embedding_model = models.Transformer(model_name)

# Apply mean pooling to get one fixed sized sentence vector
pooling_model = models.Pooling(word_embedding_model.get_word_embedding_dimension(),
                               pooling_mode_mean_tokens=True,
                               pooling_mode_cls_token=False,
                               pooling_mode_max_tokens=False)

model = SentenceTransformer(modules=[word_embedding_model, pooling_model], device=device)

train_loss = losses.CosineSimilarityLoss(model=model)

logging.info("Read dev dataset")
evaluator = EmbeddingSimilarityEvaluator.from_input_examples(dev_samples, name='data-dev')

# Configure the training. We skip evaluation in this example
warmup_steps = math.ceil(len(train_dataloader) * num_epochs  * 0.1) #10% of train data for warm-up
logging.info("Warmup-steps: {}".format(warmup_steps))

# Train the model
model.fit(train_objectives=[(train_dataloader, train_loss)],
          evaluator=evaluator,
          epochs=num_epochs,
          evaluation_steps=1000,
          warmup_steps=warmup_steps,
          output_path=model_save_path)

